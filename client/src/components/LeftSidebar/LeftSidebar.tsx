import React from 'react';
import { Link } from 'react-router-dom';

import HomeIcon from '@mui/icons-material/Home';
import TagIcon from '@mui/icons-material/Tag';
import PersonIcon from '@mui/icons-material/PersonOutline';
import NotificationsNoneIcon from '@mui/icons-material/NotificationsNone';
import MailIcon from '@mui/icons-material/MailOutline';
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder';
import ArticleOutlinedIcon from '@mui/icons-material/ArticleOutlined';
import PendingOutlinedIcon from '@mui/icons-material/PendingOutlined';

const LeftSidebar = () => {
  return (
    <div className="flex flex-col h-full md:h-[90vh] justify-between mr-6">
      <div className="mt-6 flex flex-col space-y-4">
        <Link to="/">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <HomeIcon fontSize="large" />
            <p>Home</p>
          </div>
        </Link>
        <Link to="/explore">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <TagIcon fontSize="large" />
            <p>Explore</p>
          </div>
        </Link>
        <Link to="/notifications">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <NotificationsNoneIcon fontSize="large" />
            <p>Notifications</p>
          </div>
        </Link>
        <Link to="/messages">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <MailIcon fontSize="large" />
            <p>Messages</p>
          </div>
        </Link>
        <Link to="/bookmarks">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <BookmarkBorderIcon fontSize="large" />
            <p>Bookmarks</p>
          </div>
        </Link>
        <Link to="/lists">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <ArticleOutlinedIcon fontSize="large" />
            <p>Lists</p>
          </div>
        </Link>
        <Link to="/profile/">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <PersonIcon fontSize="large" />
            <p>Profile</p>
          </div>
        </Link>
        <Link to="/more">
          <div className="flex items-center space-x-6 p-2 hover:bg-slate-200 rounded-full cursor-pointer">
            <PendingOutlinedIcon fontSize="large" />
            <p>More</p>
          </div>
        </Link>
      </div>
      <div className="flex justify-between">
        <div>
          <p className="font-bold">UserName</p>
          <p className="font-bold">@UserName</p>
        </div>
        <div>
          <Link to="signin">
            <button className="bg-red-500 px-4 py-2 text-white rounded-full">Logout</button>
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LeftSidebar;
